import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolleaversComponent } from './schoolleavers.component';

describe('SchoolleaversComponent', () => {
  let component: SchoolleaversComponent;
  let fixture: ComponentFixture<SchoolleaversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchoolleaversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolleaversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
