import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByAgeComparisonComponent } from './matexpansion-population-by-age-comparison.component';

describe('MatexpansionPopulationByAgeComparisonComponent', () => {
  let component: MatexpansionPopulationByAgeComparisonComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByAgeComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByAgeComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByAgeComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
