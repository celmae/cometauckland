import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-age-comparison',
  templateUrl: './matexpansion-population-by-age-comparison.component.html',
  styleUrls: ['./matexpansion-population-by-age-comparison.component.css']
})
export class MatexpansionPopulationByAgeComparisonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
