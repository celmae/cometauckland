import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Transition1Component } from './transition1.component';

describe('Transition1Component', () => {
  let component: Transition1Component;
  let fixture: ComponentFixture<Transition1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Transition1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Transition1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
