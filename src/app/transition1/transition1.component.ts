import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from '../services/chart-data.service';

@Component({
  selector: 'app-transition1',
  templateUrl: './transition1.component.html',
  styleUrls: ['./transition1.component.css'],

})
export class Transition1Component implements OnInit {

  // private CATEGORY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/transition1/1lBq80oa9UtSUjLF4sAAHWy3zzT2bRl9VIKptBZWTW6Y';
  populateData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(204, 255, 204)',
      borderColor: 'rgb(204, 255, 204)',
      pointBackgroundColor: 'rgb(204, 255, 204)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(204, 255, 204)'
    }
  ];

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('Transition1', 'Category')
    .subscribe(value => {
      this.categorySelection = value;
      this.selectedCategory1 = this.categorySelection[0].value;
    
      });

    this.configData.getSelection('Transition1', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

      this.populateData = this.chartDataService.transition18QualiData;
      this.mapJsonToPopulationDataChart();
    });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_maori'], selectedData1[0]['Label_pasifika'],
        selectedData1[0]['Label_asian'], selectedData1[0]['Label_melaa'],
        selectedData1[0]['Label_other'], selectedData1[0]['Label_european'],
        selectedData1[0]['Label_other']];
        this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['Maori', 'Pasifika', 'Asian', 'MELAA', 'Other', 'European', 'Auckland'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}