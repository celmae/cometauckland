import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationYearlyComponent } from './population-yearly.component';

describe('PopulationYearlyComponent', () => {
  let component: PopulationYearlyComponent;
  let fixture: ComponentFixture<PopulationYearlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationYearlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationYearlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
