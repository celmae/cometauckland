import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from '../services/chart-data.service';

@Component({
  selector: 'app-skillsandwork1-rate',
  templateUrl: './skillsandwork1-rate.component.html',
  styleUrls: ['./skillsandwork1-rate.component.css'],

})
export class Skillsandwork1RateComponent implements OnInit {

  // private CATEGORY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/skillsandwork1/1HYmEwD-bSRyzrGuzy8TO4DByGsvfsK__tN-sN2RaXfg';
  populateData: any[];

  yearSelection: any[];
  categorySelection: any[];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(241, 244, 58)',
      borderColor: 'rgb(241, 244, 58)',
      pointBackgroundColor: 'rgb(241, 244, 58)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(241, 244, 58)'
    }
  ];

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  
  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('SkillsandWorkRate', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        //  console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
             });

    this.configData.getSelection('SkillsandWorkRate', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
    
      this.populateData = this.chartDataService.skillsAndWorkLabourForceData;
      this.mapJsonToPopulationDataChart();
    });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_labourforcerate'], selectedData1[0]['Label_employmentrate'],
        selectedData1[0]['Label_unemploymentrate']];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['Labour Force', 'Employment', 'Unemployment'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}

/**
 *   const yearList = this.populateData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(JSON.stringify(yearList));
 */
