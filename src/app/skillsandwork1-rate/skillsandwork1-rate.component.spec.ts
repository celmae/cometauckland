import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Skillsandwork1RateComponent } from './skillsandwork1-rate.component';

describe('Skillsandwork1RateComponent', () => {
  let component: Skillsandwork1RateComponent;
  let fixture: ComponentFixture<Skillsandwork1RateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Skillsandwork1RateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Skillsandwork1RateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
