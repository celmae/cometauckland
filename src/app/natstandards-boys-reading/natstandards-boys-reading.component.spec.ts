import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsBoysReadingComponent } from './natstandards-boys-reading.component';

describe('NatstandardsBoysReadingComponent', () => {
  let component: NatstandardsBoysReadingComponent;
  let fixture: ComponentFixture<NatstandardsBoysReadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsBoysReadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsBoysReadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
