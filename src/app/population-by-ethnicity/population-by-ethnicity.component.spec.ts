import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByEthnicityComponent } from './population-by-ethnicity.component';

describe('PopulationByEthnicityComponent', () => {
  let component: PopulationByEthnicityComponent;
  let fixture: ComponentFixture<PopulationByEthnicityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByEthnicityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByEthnicityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
