import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByEthnicityComparisonComponent } from './population-by-ethnicity-comparison.component';

describe('PopulationByEthnicityComparisonComponent', () => {
  let component: PopulationByEthnicityComparisonComponent;
  let fixture: ComponentFixture<PopulationByEthnicityComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByEthnicityComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByEthnicityComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
