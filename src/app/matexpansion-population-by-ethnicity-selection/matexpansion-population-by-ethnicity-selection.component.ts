import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';


@Component({
  selector: 'app-matexpansion-population-by-ethnicity-selection',
  templateUrl: './matexpansion-population-by-ethnicity-selection.component.html',
  styleUrls: ['./matexpansion-population-by-ethnicity-selection.component.css']
})
export class MatexpansionPopulationByEthnicitySelectionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
