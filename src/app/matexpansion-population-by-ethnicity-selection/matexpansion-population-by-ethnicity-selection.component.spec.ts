import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByEthnicitySelectionComponent } from './matexpansion-population-by-ethnicity-selection.component';

describe('MatexpansionPopulationByEthnicitySelectionComponent', () => {
  let component: MatexpansionPopulationByEthnicitySelectionComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByEthnicitySelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByEthnicitySelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByEthnicitySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
