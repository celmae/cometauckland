import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByMigrationStackedComponent } from './matexpansion-population-by-migration-stacked.component';

describe('MatexpansionPopulationByMigrationStackedComponent', () => {
  let component: MatexpansionPopulationByMigrationStackedComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByMigrationStackedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByMigrationStackedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByMigrationStackedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
