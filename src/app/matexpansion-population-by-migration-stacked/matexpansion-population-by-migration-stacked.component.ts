import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';


@Component({
  selector: 'app-matexpansion-population-by-migration-stacked',
  templateUrl: './matexpansion-population-by-migration-stacked.component.html',
  styleUrls: ['./matexpansion-population-by-migration-stacked.component.css']
})
export class MatexpansionPopulationByMigrationStackedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
