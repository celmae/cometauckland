import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Observable";
import { ApiDataService } from "./api-data.service";

@Injectable()
export class ChartDataService {

  populationByAgeData: any[];
  populationYearlyData: any[];
  populationMigrationData: any[];
  populationEthnicityData: any[];

  earlyLearningByAgesEstimateData: any[];
  earlyLearningByAgesProjectionData: any[];
  earltyLearningByEthinicityData: any[];

  natStandardsBoysMathData: any[];
  natStandardsBoysReadingData: any[];
  natStandardsBoysWritingData: any[];

  natStandardsGirlsMathData: any[];
  natStandardsGirlsReadingData: any[];
  natStandardsGirlsWritingData: any[];
  natStandardsYr8Data: any[];

  schoolLeaversData: any[];

  skillsAndWorkLabourForceData: any[];
  skillsAndWorkLabourForceRateData: any[];
  skillsAndWorkYouthLabourForceData: any[];
  skillsAndWorkYouthLabourForceRateData: any[];

  transition18QualiData: any[];
  transitionHighestQualiData: any[];

  constructor(private apiDataService: ApiDataService) {

  }

  loadnatStandardsYr8Data() {
    this.apiDataService.getNationalStandardsYr8().subscribe(result => this.natStandardsYr8Data = result)
  }

  loadtransition18QualiData() {
    this.apiDataService.getTransition18Quali().subscribe(result => this.transition18QualiData = result)
  }


  loadtransitionHighestQualiData() {
    this.apiDataService.getTransitionHighestQuali().subscribe(result => this.transitionHighestQualiData = result)
  }


  loadskillsAndWorkYouthLabourForceData() {
    this.apiDataService.getskillsAndWorkYouthLabourForce().subscribe(result => this.skillsAndWorkYouthLabourForceData = result)
  }

  loadskillsAndWorkLabourForceData() {
    this.apiDataService.getskillsAndWorkLabourForce().subscribe(result => this.skillsAndWorkLabourForceData = result)
  }


  loadschoolLeaversData() {
    this.apiDataService.getSchoolLeavers().subscribe(result => this.schoolLeaversData = result)
  }


  loadnatStandardsGirlsMathData() {
    this.apiDataService.getNationalStandardsGirlsMath().subscribe(result => this.natStandardsGirlsMathData = result)
  }

  loadnatStandardsGirlsReadingData() {
    this.apiDataService.getNationalStandardsGirlsReading().subscribe(result => this.natStandardsGirlsReadingData = result)
  }


  loadnatStandardsGirlsWritingData() {
    this.apiDataService.getNationalStandardsGirlsWriting().subscribe(result => this.natStandardsGirlsWritingData = result)
  }

  loadnatStandardsBoysWritingData() {
    this.apiDataService.getNationalStandardsBoysWriting().subscribe(result => this.natStandardsBoysWritingData = result)
  }

  loadnatStandardsBoysMathData() {
    this.apiDataService.getNationalStandardsBoysMath().subscribe(result => this.natStandardsBoysMathData = result)
  }

  loadStandardsBoysReadingData() {
    this.apiDataService.getNationalStandardsBoysReading().subscribe(result => this.natStandardsBoysReadingData = result)
  }

  loadEarlyLearningByEthinicityData() {
    this.apiDataService.getEarlyLearningByEthinicity().subscribe(result => this.earltyLearningByEthinicityData = result)
  }


  loadEarlyLearningByAgesProjectionData() {
    this.apiDataService.getEarlyLearningByAgesProjections().subscribe(result => this.earlyLearningByAgesProjectionData = result)
  }


  loadEarlyLearningByAgesEstimateData() {
    this.apiDataService.getEarlyLearningByAgesEstimates().subscribe(result => this.earlyLearningByAgesEstimateData = result)
  }

  loadPopulatioByEthnicityData() {
    this.apiDataService.getPopulationByEthnicity().subscribe(result => this.populationEthnicityData = result);
  }

  loadPopulationByMgrationData() {
    this.apiDataService.getPopulationByMigration().subscribe(result => this.populationMigrationData = result);
  }

  loadPopulationByAgeData() {
    this.apiDataService.getPopulationByAge().subscribe(result => this.populationByAgeData = result);
  }


  loadPopulationYearlyData() {
    this.apiDataService.getPopulationYearly().subscribe(result => this.populationYearlyData = result);
  }

}
