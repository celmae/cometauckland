import { TestBed, inject } from '@angular/core/testing';

import { GraphDatabaseJsonserviceService } from './graph-database-jsonservice.service';

describe('GraphDatabaseJsonserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GraphDatabaseJsonserviceService]
    });
  });

  it('should be created', inject([GraphDatabaseJsonserviceService], (service: GraphDatabaseJsonserviceService) => {
    expect(service).toBeTruthy();
  }));
});
