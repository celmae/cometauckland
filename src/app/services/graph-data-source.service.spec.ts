import { TestBed, inject } from '@angular/core/testing';

import { GraphDataSourceService } from './graph-data-source.service';

describe('GraphDataSourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GraphDataSourceService]
    });
  });

  it('should be created', inject([GraphDataSourceService], (service: GraphDataSourceService) => {
    expect(service).toBeTruthy();
  }));
});
