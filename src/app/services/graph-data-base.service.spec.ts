import { TestBed, inject } from '@angular/core/testing';

import { GraphDataBaseService } from './graph-data-base.service';

describe('GraphDataBaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GraphDataBaseService]
    });
  });

  it('should be created', inject([GraphDataBaseService], (service: GraphDataBaseService) => {
    expect(service).toBeTruthy();
  }));
});
