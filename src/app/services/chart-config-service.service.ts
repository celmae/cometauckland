import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiDataService } from "./api-data.service";
import 'rxjs/add/observable/of';

@Injectable()
export class ChartConfigService {

  chartConfigData: any

  constructor(private apiDataService: ApiDataService) { }


  loadChartConfigData() {
    this.chartConfigData = this.apiDataService.getChartConfig().subscribe(result => this.chartConfigData = result);
  }

  public getSelection(chart: string, selection: string): Observable<any[]> {

    return Observable.of(this.chartConfigData
      .filter(value => value['chart'] == chart && value['selection'] == selection));

  }


  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.messagen || error);
  }

}
