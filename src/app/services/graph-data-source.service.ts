import { Injectable } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { ChartItem } from './models/chart-item';
import { GraphDataBaseService } from './graph-data-base.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Injectable()
export class GraphDataSourceService extends DataSource<any> {

  _filterYear = new BehaviorSubject('');
  get filterYear(): string { return this._filterYear.value; }
  set filterYear(filterYear: string) {
    this._filterYear.next(filterYear);
  }

  // private _filterYear: string;
  // get filterYear(): string { return this._filterYear; }
  // set filterYear(filterYear: string) { this._filterYear = filterYear; }

  constructor(private _db: GraphDataBaseService) {
    super();
  }

  public show(): string {
    return this._filterYear.value;
  }

  connect(): Observable<ChartItem[]> {
    return this._db.PopulationData
      .map(this.filter)
      .catch(this.handleError);
  }
  private filter(result: ChartItem[]) {
    console.log(this.filterYear);
    return result.filter(e => e.label2 = '2013');
  }
  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  disconnect() { }
}
