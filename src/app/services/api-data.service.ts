import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ApiDataService {

  private EARLY_LEARNING_BY_ETHINICITY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/earlylearning-by-ethnicity/1VNfRscVRipErWNRBW1z06IfDXoTanSwCmQ59nofki0M';

  private EARLY_LEARNING_BY_AGES_PROJECTIONS_API_URL = "https://s3.amazonaws.com/chartdata.cometauckland.org.nz/earlylearning-by-ages-projections/1hJUPw8i4OHx52IQDT36RWPVS22r1OlgfRyQyXPJsc7Y";

  private EARLY_LEARNING_BY_AGES_ESTIMATES_API_URL = "https://s3.amazonaws.com/chartdata.cometauckland.org.nz/earlylearning-by-ages-estimates/18mmys_j-Mmdr78PBCOgkdfNeaKUpfRnBNanqxkNckUA";

  private POPULATION_BY_AGE_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-age/1pPOMrGJUBfRjFzFfjjfG-k7XuEUO11FKQIiota4XFKo';

  private POPULATION_BY_YEARLY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-yearly/13IiIllmyS2HBBsYFbR5eMf9jXBRJrK7qnPCAdZLukfk';

  private POPULATION_BY_MIGRATION_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-migration/1RgK3s7pcEQVpcuw1TNejdcBuaTOJYRLIAkrpuYflUzc';

  private POPULATION_BY_ETHNICITY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-ethnicity/1J53OMyfq3hICF73hlGTEp1TDw2eUXgIIRjQXLlRUb4g';

  private NATSTANDARDS_BOYS_MATH_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-boys(math)/1j2MnS0KazxmgzETuwpTZdPxbEBXHJXS5-i8V-s1-CpM';

  private NATSTANDARDS_BOYS_READING_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-boys(reading)/1aggja3-FoNDFF1-wY19SEA24ht47LvVBLw1XfxFV64E';

  private NATSTANDARDS_BOYS_WRITING_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-boys(writing)/1_ICSfmTs1IgNLeUAQKXveo0mnOd8I6yG2npehVToWlw';
  
private NATSTANDARDS_GIRLS_MATH_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-girls(math)/1vt4fZfWtAcq9JhcVYDirGFQdDrVhEwfOPr34SiCJbqg';

private NATSTANDARDS_GIRLS_READING_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-girls(reading)/1IgpMlBsQpLdh1I24CZoNfStzHQuJLB-90X-Smva7EMI';

private NATSTANDARDS_GIRLS_WRITING_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-girls(writing)/1uRQcQgPCDoHkVAv7yc5urPsab7KD8_VCDT3BOSL7JfE';

private SCHOOL_LEAVERS_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/schoolleavers/1SfCMTzMdG3t83yJcLfUQ_6uhrPSXyGWr3mJLWmu4fv4';

private SKILLSANDWORK_LABOUR_FORCE_API_URL =  'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/skillsandwork1/1HYmEwD-bSRyzrGuzy8TO4DByGsvfsK__tN-sN2RaXfg';

private SKILLSANDWORK_YOUTH_LABOUR_FORCE_API_URL  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/skillsandwork2/12O6Wskxrhovi_zTIQvxyEoQ_0_b3ezMFpk9qDb06QeQ';

private TRANSITION_18_WITH_QUAL_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/transition1/1lBq80oa9UtSUjLF4sAAHWy3zzT2bRl9VIKptBZWTW6Y';

private TRANSITION_HIGHEST_QUAL_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/transition2/1-FHlr4TErn-zV0mGB6rLkn_AyyzBFj7Atlouw7FvoCA';

private NATSTANDARDS_YR8_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandardsy8/1HBreIBOEg3NPa9qEgs4av65hZ9wsaxJAsFjYPJz2NF0';

private CHART_CONFIG_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/configuration/1Dd3JIdHB8mKVax1t2Dyig1d2ZPiDHzKQGtmJ5Prarnc';


  constructor(private http: Http) { }


  public getChartConfig(): Observable<any[]> {
    return this.http
      .get(this.CHART_CONFIG_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  
  public getNationalStandardsYr8(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_YR8_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getTransition18Quali(): Observable<any[]> {
    return this.http
      .get(this.TRANSITION_18_WITH_QUAL_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  public getTransitionHighestQuali(): Observable<any[]> {
    return this.http
      .get(this.TRANSITION_HIGHEST_QUAL_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getskillsAndWorkYouthLabourForce(): Observable<any[]> {
    return this.http
      .get(this.SKILLSANDWORK_YOUTH_LABOUR_FORCE_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  public getskillsAndWorkLabourForce(): Observable<any[]> {
    return this.http
      .get(this.SKILLSANDWORK_LABOUR_FORCE_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  
  public getSchoolLeavers(): Observable<any[]> {
    return this.http
      .get(this.SCHOOL_LEAVERS_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getNationalStandardsGirlsReading(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_GIRLS_READING_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  public getNationalStandardsGirlsWriting(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_GIRLS_WRITING_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  public getNationalStandardsGirlsMath(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_GIRLS_MATH_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getNationalStandardsBoysWriting(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_BOYS_WRITING_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getNationalStandardsBoysReading(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_BOYS_READING_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getNationalStandardsBoysMath(): Observable<any[]> {
    return this.http
      .get(this.NATSTANDARDS_BOYS_MATH_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getEarlyLearningByEthinicity(): Observable<any[]> {
    return this.http
      .get(this.EARLY_LEARNING_BY_ETHINICITY_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getEarlyLearningByAgesProjections(): Observable<any[]> {
    return this.http
      .get(this.EARLY_LEARNING_BY_AGES_PROJECTIONS_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getEarlyLearningByAgesEstimates(): Observable<any[]> {
    return this.http
      .get(this.EARLY_LEARNING_BY_AGES_ESTIMATES_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getPopulationByEthnicity(): Observable<any[]> {
    return this.http
      .get(this.POPULATION_BY_ETHNICITY_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }



  public getPopulationByMigration(): Observable<any[]> {
    return this.http
      .get(this.POPULATION_BY_MIGRATION_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }


  public getPopulationByAge(): Observable<any[]> {
    return this.http
      .get(this.POPULATION_BY_AGE_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  public getPopulationYearly(): Observable<any[]> {
    return this.http
      .get(this.POPULATION_BY_YEARLY_API_URL)
      .map(res => res.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.messagen || error);
  }


}
