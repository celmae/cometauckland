import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByMigrationComparisonComponent } from './population-by-migration-comparison.component';

describe('PopulationByMigrationComparisonComponent', () => {
  let component: PopulationByMigrationComparisonComponent;
  let fixture: ComponentFixture<PopulationByMigrationComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByMigrationComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByMigrationComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
