import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";


@Component({
  selector: 'app-population-by-migration-comparison',
  templateUrl: './population-by-migration-comparison.component.html',
  styleUrls: ['./population-by-migration-comparison.component.css'],
})
export class PopulationByMigrationComparisonComponent implements OnInit {

  populateData: any[];

  yearSelection: any[];
  categorySelection: any[];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];

  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(244, 150, 0)',
      borderColor: 'rgb(244, 150, 0)',
      pointBackgroundColor: 'rgb(244, 150, 0)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(244, 150, 0)'
    },
    {
      backgroundColor: 'rgb(122, 228, 149)',
      borderColor: 'rgb(122, 228, 149)',
      pointBackgroundColor: 'rgb(122, 228, 149)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(122, 228, 149)'
    }



  ];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByMigration', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        // console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
        this.selectedCategory2 = this.categorySelection[1].value;
      });

    this.configData.getSelection('PopulationByMigration', 'Year')
      .subscribe(value => {

        //  console.log(JSON.stringify(this.yearSelection));
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

        //Load chart data
        this.populateData = this.chartDataService.populationMigrationData;
        this.mapJsonToPopulationDataChart()
      });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }


  private mapJsonToPopulationDataChart() {

    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    const selectedData2 = this.populateData
      .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_Total'], selectedData1[0]['Label_naturalincrease'], selectedData1[0]['Label_netmigration']
      ];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    if (selectedData2.length > 0) {
      const data2 = [
        selectedData2[0]['Label_Total'], selectedData2[0]['Label_naturalincrease'], selectedData2[0]['Label_netmigration']
      ];
      this.chartData.push({ 'data': data2, 'label': this.selectedCategory2 });

    }
    this.chartLabels = ['Total', 'Natural Increase', 'Net Migration'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}

