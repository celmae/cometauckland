import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabourforceComponent } from './labourforce.component';

describe('LabourforceComponent', () => {
  let component: LabourforceComponent;
  let fixture: ComponentFixture<LabourforceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabourforceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabourforceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
