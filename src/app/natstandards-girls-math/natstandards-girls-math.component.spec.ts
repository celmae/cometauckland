import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsGirlsMathComponent } from './natstandards-girls-math.component';

describe('NatstandardsGirlsMathComponent', () => {
  let component: NatstandardsGirlsMathComponent;
  let fixture: ComponentFixture<NatstandardsGirlsMathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsGirlsMathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsGirlsMathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
