import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsReadingComponent } from './natstandards-reading.component';

describe('NatstandardsReadingComponent', () => {
  let component: NatstandardsReadingComponent;
  let fixture: ComponentFixture<NatstandardsReadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsReadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsReadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
