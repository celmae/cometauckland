import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByAgeComparisonComponent } from './population-by-age-comparison.component';

describe('PopulationByAgeComparisonComponent', () => {
  let component: PopulationByAgeComparisonComponent;
  let fixture: ComponentFixture<PopulationByAgeComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByAgeComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByAgeComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
