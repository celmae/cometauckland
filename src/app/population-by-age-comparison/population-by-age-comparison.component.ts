import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";


@Component({
  selector: 'app-population-by-age-comparison',
  templateUrl: './population-by-age-comparison.component.html',
  styleUrls: ['./population-by-age-comparison.component.css'],

})
export class PopulationByAgeComparisonComponent implements OnInit {

  populateData: any[];
  yearSelection: any[];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];

  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(102, 102, 255)',
      borderColor: 'rgb(102, 102, 255)',
      pointBackgroundColor: 'rgb(102, 102, 255)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(102, 102, 255)'
    },
    {
      backgroundColor: 'rgb(0, 244, 169)',
      borderColor: 'rgb(0, 244, 169)',
      pointBackgroundColor: 'rgb(0, 244, 169)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(0, 244, 169)'
    }

  ];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private configData: ChartConfigService, private chartDataService: ChartDataService) { }

  ngOnInit() {
    this.configData.getSelection('PopulationByAges-Compare', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        //    console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
        this.selectedCategory2 = this.categorySelection[1].value;
      });

    this.configData.getSelection('PopulationByAges-Compare', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

        //load chart data after loading dropdown data
        this.populateData = this.chartDataService.populationByAgeData;
        this.mapJsonToPopulationDataChart(this.populateData);
      });

  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart(this.populateData);
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart(this.populateData);
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart(this.populateData);
  }


  private mapJsonToPopulationDataChart(data: any[]) {
    this.populateData = data;
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    const selectedData2 = this.populateData
      .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_Total'], selectedData1[0]['Label_014'], selectedData1[0]['Label_1539'],
        selectedData1[0]['Label_4064'], selectedData1[0]['Label_65']
      ];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    if (selectedData2.length > 0) {
      const data2 = [
        selectedData2[0]['Label_Total'], selectedData2[0]['Label_014'], selectedData2[0]['Label_1539'],
        selectedData2[0]['Label_4064'], selectedData2[0]['Label_65']
      ];
      this.chartData.push({ 'data': data2, 'label': this.selectedCategory2 });

    }
    this.chartLabels = ['Total', '0-14', '15-39', '40-64', '65'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}

/**
 *   const yearList = this.populateData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(JSON.stringify(yearList));
 */
