import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByAgeComponent } from './matexpansion-population-by-age.component';

describe('MatexpansionPopulationByAgeComponent', () => {
  let component: MatexpansionPopulationByAgeComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByAgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByAgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByAgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
