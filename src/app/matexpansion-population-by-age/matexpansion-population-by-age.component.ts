import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-age',
  templateUrl: './matexpansion-population-by-age.component.html',
  styleUrls: ['./matexpansion-population-by-age.component.css']
})
export class MatexpansionPopulationByAgeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
