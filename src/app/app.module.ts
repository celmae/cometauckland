import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatTabsModule,
  MatTableModule,
  MatSelectModule,
  MatExpansionModule,
  MatInputModule,
  MatFormFieldModule,
  MatToolbarModule,

} from '@angular/material';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';
import { TabsComponent } from './tabs/tabs.component';
import { PopulationByAgeComponent } from './population-by-age/population-by-age.component';
import { PopulationByEthnicityComponent } from './population-by-ethnicity/population-by-ethnicity.component';
import { PopulationByMigrationComponent } from './population-by-migration/population-by-migration.component';
import { PopulationYearlyComponent } from './population-yearly/population-yearly.component';
import { PopulationByAgeComparisonComponent } from './population-by-age-comparison/population-by-age-comparison.component';
import { PopulationByEthnicityComparisonComponent } from './population-by-ethnicity-comparison/population-by-ethnicity-comparison.component';
import { PopulationByMigrationComparisonComponent } from './population-by-migration-comparison/population-by-migration-comparison.component';
import { PopulationByYearlyComparisonComponent } from './population-by-yearly-comparison/population-by-yearly-comparison.component';
import { PopulationByEthnicitySelectionComponent } from './population-by-ethnicity-selection/population-by-ethnicity-selection.component';
import { PopulationByMigrationStackedchartComponent } from './population-by-migration-stackedchart/population-by-migration-stackedchart.component';
import { EarlylearningByAgesEstimatesComponent } from './earlylearning-by-ages-estimates/earlylearning-by-ages-estimates.component';
import { EarlylearningByAgesProjectionsComponent } from './earlylearning-by-ages-projections/earlylearning-by-ages-projections.component';
import { EarlylearningByEthnicityComponent } from './earlylearning-by-ethnicity/earlylearning-by-ethnicity.component';
import { ChartConfigService } from './services/chart-config-service.service';
import { Transition1Component } from './transition1/transition1.component';
import { Transition2Component } from './transition2/transition2.component';
import { Skillsandwork1Component } from './skillsandwork1/skillsandwork1.component';
import { NatstandardsReadingComponent } from './natstandards-reading/natstandards-reading.component';
import { Skillsandwork2Component } from './skillsandwork2/skillsandwork2.component';
import { Skillsandwork1RateComponent } from './skillsandwork1-rate/skillsandwork1-rate.component';
import { NatstandardsYr8Component } from './natstandards-yr8/natstandards-yr8.component';
import { SchoolleaversComponent } from './schoolleavers/schoolleavers.component';
import { NatstandardsGirlsReadingComponent } from './natstandards-girls-reading/natstandards-girls-reading.component';
import { NatstandardsGirlsWritingComponent } from './natstandards-girls-writing/natstandards-girls-writing.component';
import { NatstandardsGirlsMathComponent } from './natstandards-girls-math/natstandards-girls-math.component';
import { NatstandardsBoysReadingComponent } from './natstandards-boys-reading/natstandards-boys-reading.component';
import { NatstandardsBoysWritingComponent } from './natstandards-boys-writing/natstandards-boys-writing.component';
import { NatstandardsBoysMathComponent } from './natstandards-boys-math/natstandards-boys-math.component';
import { YouthlabourforcerateComponent } from './youthlabourforcerate/youthlabourforcerate.component';
import { ApiDataService } from "./services/api-data.service";
import { ChartDataService } from "./services/chart-data.service";
import { LabourforceComponent } from './labourforce/labourforce.component';

@NgModule({
  declarations: [
    AppComponent,
    TabsComponent,
    PopulationByAgeComponent,
    PopulationByEthnicityComponent,
    PopulationByMigrationComponent,
    PopulationYearlyComponent,
    PopulationByAgeComparisonComponent,
    PopulationByEthnicityComparisonComponent,
    PopulationByMigrationComparisonComponent,
    PopulationByYearlyComparisonComponent,
    PopulationByEthnicitySelectionComponent,
    PopulationByMigrationStackedchartComponent,
    EarlylearningByAgesEstimatesComponent,
    EarlylearningByAgesProjectionsComponent,
    EarlylearningByEthnicityComponent,
    Transition1Component,
    Transition2Component,
    Skillsandwork1Component,
    NatstandardsReadingComponent,
    Skillsandwork2Component,
    Skillsandwork1RateComponent,
    NatstandardsYr8Component,
    SchoolleaversComponent,
    NatstandardsGirlsReadingComponent,
    NatstandardsGirlsWritingComponent,
    NatstandardsGirlsMathComponent,
    NatstandardsBoysReadingComponent,
    NatstandardsBoysWritingComponent,
    NatstandardsBoysMathComponent,
    YouthlabourforcerateComponent,
    LabourforceComponent


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    CommonModule,
    ChartsModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatToolbarModule
  ],
  providers: [ChartConfigService,ApiDataService,ChartDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
