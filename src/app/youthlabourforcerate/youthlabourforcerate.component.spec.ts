import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YouthlabourforcerateComponent } from './youthlabourforcerate.component';

describe('YouthlabourforcerateComponent', () => {
  let component: YouthlabourforcerateComponent;
  let fixture: ComponentFixture<YouthlabourforcerateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YouthlabourforcerateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YouthlabourforcerateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
