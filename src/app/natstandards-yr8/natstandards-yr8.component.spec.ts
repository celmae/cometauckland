import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsYr8Component } from './natstandards-yr8.component';

describe('NatstandardsYr8Component', () => {
  let component: NatstandardsYr8Component;
  let fixture: ComponentFixture<NatstandardsYr8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsYr8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsYr8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
