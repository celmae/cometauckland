import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from '../services/chart-data.service';

@Component({
  selector: 'app-natstandards-yr8',
  templateUrl: './natstandards-yr8.component.html',
  styleUrls: ['./natstandards-yr8.component.css'],

})
export class NatstandardsYr8Component implements OnInit {

  // private CATEGORY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandardsy8/1HBreIBOEg3NPa9qEgs4av65hZ9wsaxJAsFjYPJz2NF0';
  populateData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };


  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(179, 217, 255)',
      borderColor: 'rgb(179, 217, 255)',
      pointBackgroundColor: 'rgb(179, 217, 255)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(179, 217, 255)'
    },
    {
      backgroundColor: 'rgb(179, 255, 217)',
      borderColor: 'rgb(179, 255, 217)',
      pointBackgroundColor: 'rgb(179, 255, 217)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(179, 255, 217)'
    }

  ];

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }
  
  ngOnInit() {

    this.configData.getSelection('NatStandardsYr8', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        //  console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
      });

    this.configData.getSelection('NatStandardsYr8', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
    
      this.populateData = this.chartDataService.natStandardsYr8Data;
      this.mapJsonToPopulationDataChart();
    });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_reading'], selectedData1[0]['Label_writing'], selectedData1[0]['Label_math']
      ];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['Reading', 'Writing', 'Math'];
    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}

/**
 *   const yearList = this.populateData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(JSON.stringify(yearList));
 */
