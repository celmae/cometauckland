import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByEthnicityComparisonComponent } from './matexpansion-population-by-ethnicity-comparison.component';

describe('MatexpansionPopulationByEthnicityComparisonComponent', () => {
  let component: MatexpansionPopulationByEthnicityComparisonComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByEthnicityComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByEthnicityComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByEthnicityComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
