import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-ethnicity-comparison',
  templateUrl: './matexpansion-population-by-ethnicity-comparison.component.html',
  styleUrls: ['./matexpansion-population-by-ethnicity-comparison.component.css']
})
export class MatexpansionPopulationByEthnicityComparisonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
