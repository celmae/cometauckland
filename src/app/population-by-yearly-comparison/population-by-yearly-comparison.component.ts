import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";

@Component({
  selector: 'app-population-by-yearly-comparison',
  templateUrl: './population-by-yearly-comparison.component.html',
  styleUrls: ['./population-by-yearly-comparison.component.css'],

})
export class PopulationByYearlyComparisonComponent implements OnInit {

  populateData: any[];
  categorySelection: any[];

  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'line';
  chartLegend: boolean = true;
  chartData: any[];



  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationYearly', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        // console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
        this.selectedCategory2 = this.categorySelection[1].value;

        this.populateData = this.chartDataService.populationYearlyData;
        this.mapJsonToPopulationDataChart();
      });


  }


  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {

    this.chartData = [];
    this.chartLabels = [];

    // Find all years
    const yearList = this.populateData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    // console.log(JSON.stringify(yearList));

    // find all location and year using first  category
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && yearList.includes(value['Year'])));

    // console.log(JSON.stringify(selectedData1));
    const data1 = [];
    selectedData1.forEach(value => {
      data1.push(value['Label_population']);
    });

    if (selectedData1.length > 0) {
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }


    // find all location and year using second category
    const selectedData2 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory2 && yearList.includes(value['Year'])));

    // console.log(JSON.stringify(selectedData2));
    const data2 = [];
    selectedData2.forEach(value => {
      data2.push(value['Label_population']);
    });

    if (selectedData2.length > 0) {
      this.chartData.push({ 'data': data2, 'label': this.selectedCategory2 });
    }

    // put the year in the labels
    yearList.forEach(value => {
      this.chartLabels.push(String(value));
    });

    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}
