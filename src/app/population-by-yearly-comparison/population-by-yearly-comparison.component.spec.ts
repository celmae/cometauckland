import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByYearlyComparisonComponent } from './population-by-yearly-comparison.component';

describe('PopulationByYearlyComparisonComponent', () => {
  let component: PopulationByYearlyComparisonComponent;
  let fixture: ComponentFixture<PopulationByYearlyComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByYearlyComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByYearlyComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
