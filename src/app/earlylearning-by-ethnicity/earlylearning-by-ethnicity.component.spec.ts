import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarlylearningByEthnicityComponent } from './earlylearning-by-ethnicity.component';

describe('EarlylearningByEthnicityComponent', () => {
  let component: EarlylearningByEthnicityComponent;
  let fixture: ComponentFixture<EarlylearningByEthnicityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarlylearningByEthnicityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarlylearningByEthnicityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
