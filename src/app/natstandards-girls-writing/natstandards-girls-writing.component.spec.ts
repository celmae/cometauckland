import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsGirlsWritingComponent } from './natstandards-girls-writing.component';

describe('NatstandardsGirlsWritingComponent', () => {
  let component: NatstandardsGirlsWritingComponent;
  let fixture: ComponentFixture<NatstandardsGirlsWritingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsGirlsWritingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsGirlsWritingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
