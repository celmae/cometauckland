import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Skillsandwork2Component } from './skillsandwork2.component';

describe('Skillsandwork2Component', () => {
  let component: Skillsandwork2Component;
  let fixture: ComponentFixture<Skillsandwork2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Skillsandwork2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Skillsandwork2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
