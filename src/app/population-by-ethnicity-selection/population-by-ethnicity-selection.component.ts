import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";

@Component({
  selector: 'app-population-by-ethnicity-selection',
  templateUrl: './population-by-ethnicity-selection.component.html',
  styleUrls: ['./population-by-ethnicity-selection.component.css'],
})
export class PopulationByEthnicitySelectionComponent implements OnInit {

  populateData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'pie';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByEthnicity-Compare', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        //  console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
        this.selectedCategory2 = this.categorySelection[1].value;
      });

    this.configData.getSelection('PopulationByEthnicity-Compare', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
       
        //load chart data
        this.populateData = this.chartDataService.populationEthnicityData;
        this.mapJsonToPopulationDataChart();
      });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_european'], selectedData1[0]['Label_maori'],
        selectedData1[0]['Label_asian'], selectedData1[0]['Label_pacific']];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['European or Other', 'Maori', 'Asian', 'Pacific'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}

/**
 *   const yearList = this.populateData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(JSON.stringify(yearList));
 */
