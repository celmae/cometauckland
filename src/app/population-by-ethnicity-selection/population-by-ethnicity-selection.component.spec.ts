import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByEthnicitySelectionComponent } from './population-by-ethnicity-selection.component';

describe('PopulationByEthnicitySelectionComponent', () => {
  let component: PopulationByEthnicitySelectionComponent;
  let fixture: ComponentFixture<PopulationByEthnicitySelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByEthnicitySelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByEthnicitySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
