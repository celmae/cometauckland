import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Skillsandwork1Component } from './skillsandwork1.component';

describe('Skillsandwork1Component', () => {
  let component: Skillsandwork1Component;
  let fixture: ComponentFixture<Skillsandwork1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Skillsandwork1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Skillsandwork1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
