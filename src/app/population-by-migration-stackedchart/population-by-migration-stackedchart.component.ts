import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";

@Component({
  selector: 'app-population-by-migration-stackedchart',
  templateUrl: './population-by-migration-stackedchart.component.html',
  styleUrls: ['./population-by-migration-stackedchart.component.css'],

})
export class PopulationByMigrationStackedchartComponent implements OnInit {

  populateData: any[];
  yearSelection: any[];
  selectedYear: string;

  chartLabels: any[];
  chartType: string = 'horizontalBar';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }


  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDatService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByMigration', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

        //Load chart data
        this.populateData = this.chartDatService.populationMigrationData;
        this.mapJsonToPopulationDataChart();
      });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // Select data based on the Year selected
    const selectedData = this.populateData.filter(value => (value['Year'] == this.selectedYear))
    // Exclude New Zealand and Auckland
    const filtereData = selectedData.filter(value => (value['Location'] != 'New Zealand' && value['Location'] != 'Auckland'));
    // console.log(JSON.stringify(selectedData));
    const data1 = [];
    const data2 = [];
    // Put all labels using Location property and get the data1 and data2
    filtereData.forEach(value => {
      this.chartLabels.push(value['Location']);
      data1.push(value['Label_naturalincrease']);
      data2.push(value['Label_netmigration']);
    });
    // Construct chart data
    this.chartData.push({ 'data': data1, 'label': 'Natural Increase' });
    this.chartData.push({ 'data': data2, 'label': 'Net Migration' });

    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}
