import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByMigrationStackedchartComponent } from './population-by-migration-stackedchart.component';

describe('PopulationByMigrationStackedchartComponent', () => {
  let component: PopulationByMigrationStackedchartComponent;
  let fixture: ComponentFixture<PopulationByMigrationStackedchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByMigrationStackedchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByMigrationStackedchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
