import { Component, OnInit } from '@angular/core';
import { ChartDataService } from "./services/chart-data.service";
import { ChartConfigService } from "./services/chart-config-service.service";
import {MatButtonModule} from '@angular/material/button';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'app';
  constructor(public chartDataService: ChartDataService, public chartConfigService : ChartConfigService) {

  }
  ngOnInit(): void {
    
    this.chartConfigService.loadChartConfigData();
    
    this.chartDataService.loadPopulatioByEthnicityData()
    this.chartDataService.loadPopulationByAgeData();
    this.chartDataService.loadPopulationYearlyData();
    this.chartDataService.loadPopulationByMgrationData();
    this.chartDataService.loadEarlyLearningByAgesEstimateData();
    this.chartDataService.loadEarlyLearningByAgesProjectionData();
    this.chartDataService.loadEarlyLearningByEthinicityData();
    this.chartDataService.loadnatStandardsBoysMathData();
    this.chartDataService.loadnatStandardsBoysWritingData();
    this.chartDataService.loadStandardsBoysReadingData();
    this.chartDataService.loadnatStandardsGirlsMathData();
    this.chartDataService.loadnatStandardsGirlsReadingData();
    this.chartDataService.loadnatStandardsGirlsWritingData();
    this.chartDataService.loadnatStandardsYr8Data();
    this.chartDataService.loadschoolLeaversData();
    this.chartDataService.loadskillsAndWorkLabourForceData();
    this.chartDataService.loadskillsAndWorkYouthLabourForceData();
    this.chartDataService.loadtransition18QualiData();
    this.chartDataService.loadtransitionHighestQualiData();
  
  }

}
