import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-ethnicity',
  templateUrl: './matexpansion-population-by-ethnicity.component.html',
  styleUrls: ['./matexpansion-population-by-ethnicity.component.css']
})
export class MatexpansionPopulationByEthnicityComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
