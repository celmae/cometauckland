import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from "../services/chart-data.service";


@Component({
  selector: 'app-population-by-migration',
  templateUrl: './population-by-migration.component.html',
  styleUrls: ['./population-by-migration.component.css'],
})
export class PopulationByMigrationComponent implements OnInit {

  populateData: any[];
  yearSelection: any[];
  categorySelection: any[];

  selectedYear: string;
  selectedCategory: string;

  chartLabels: any[];
  chartType: string = 'horizontalBar';
  chartLegend: boolean = true;
  chartData: any[];

  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartColors: any[] = [
    {
      backgroundColor: 'rgb(255, 204, 153)',
      borderColor: 'rgb(255, 204, 153)',
      pointBackgroundColor: 'rgb(255, 204, 153)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(255, 204, 153)'
    }
  ];

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByMigration', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

        //load chart data
        this.populateData = this.chartDataService.populationMigrationData
        this.mapJsonToPopulationDataChart();

      });

  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory(categorySelection) {
    this.selectedCategory = categorySelection.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct fieldname in googlesheets/json, remove minus(-),  JSON field name must not contain -
    const property = 'Label_' + this.selectedCategory.replace('-', '');

    // find all JSON with selected  year
    const filteredData = this.populateData
      .filter(value => (value['Year'] == this.selectedYear))
      // remove Auckland and New Zealand
      .filter(value => value['Location'] != 'New Zealand' && value['Location'] != 'Auckland');
    // Copy location to array of labels
    filteredData.forEach(value => (this.chartLabels.push(value['Location'])));
    // Copy the values to chart data
    const data = [];
    filteredData.forEach(value => (data.push(value[property])));
    this.chartData.push({ 'data': data, 'label': String(this.selectedYear) });

    // console.log(JSON.stringify(this.chartLabels));
    // console.log(JSON.stringify(this.chartData));
  }
}

