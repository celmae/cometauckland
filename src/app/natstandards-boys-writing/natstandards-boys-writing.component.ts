import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from '../services/chart-data.service';

@Component({
  selector: 'app-natstandards-boys-writing',
  templateUrl: './natstandards-boys-writing.component.html',
  styleUrls: ['./natstandards-boys-writing.component.css'],

})
export class NatstandardsBoysWritingComponent implements OnInit {

  // private CATEGORY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/natstandards-boys(writing)/1_ICSfmTs1IgNLeUAQKXveo0mnOd8I6yG2npehVToWlw';
  populateData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'pie';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

 

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }
  
  ngOnInit() {

    this.configData.getSelection('NatStandardsBoys-Writing', 'Category')
    .subscribe(value => {
      this.categorySelection = value;
      this.selectedCategory1 = this.categorySelection[0].value;
    
      });

    this.configData.getSelection('NatStandardsBoys-Writing', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
  
   
      this.populateData = this.chartDataService.natStandardsBoysWritingData;
      this.mapJsonToPopulationDataChart();
    });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_wellbelow'], selectedData1[0]['Label_below'],
        selectedData1[0]['Label_at'], selectedData1[0]['Label_above']];
        this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['Well Below', 'Below', 'At', 'Above'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}