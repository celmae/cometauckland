import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsBoysWritingComponent } from './natstandards-boys-writing.component';

describe('NatstandardsBoysWritingComponent', () => {
  let component: NatstandardsBoysWritingComponent;
  let fixture: ComponentFixture<NatstandardsBoysWritingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsBoysWritingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsBoysWritingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
