import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarlylearningByAgesProjectionsComponent } from './earlylearning-by-ages-projections.component';

describe('EarlylearningByAgesProjectionsComponent', () => {
  let component: EarlylearningByAgesProjectionsComponent;
  let fixture: ComponentFixture<EarlylearningByAgesProjectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarlylearningByAgesProjectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarlylearningByAgesProjectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
