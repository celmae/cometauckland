import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-yearly-comparison',
  templateUrl: './matexpansion-population-yearly-comparison.component.html',
  styleUrls: ['./matexpansion-population-yearly-comparison.component.css']
})
export class MatexpansionPopulationYearlyComparisonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
