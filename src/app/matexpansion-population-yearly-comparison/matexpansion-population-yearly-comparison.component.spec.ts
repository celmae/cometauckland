import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationYearlyComparisonComponent } from './matexpansion-population-yearly-comparison.component';

describe('MatexpansionPopulationYearlyComparisonComponent', () => {
  let component: MatexpansionPopulationYearlyComparisonComponent;
  let fixture: ComponentFixture<MatexpansionPopulationYearlyComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationYearlyComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationYearlyComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
