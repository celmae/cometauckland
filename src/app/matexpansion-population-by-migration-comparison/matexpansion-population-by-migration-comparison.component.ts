import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-migration-comparison',
  templateUrl: './matexpansion-population-by-migration-comparison.component.html',
  styleUrls: ['./matexpansion-population-by-migration-comparison.component.css']
})
export class MatexpansionPopulationByMigrationComparisonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
