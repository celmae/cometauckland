import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByMigrationComparisonComponent } from './matexpansion-population-by-migration-comparison.component';

describe('MatexpansionPopulationByMigrationComparisonComponent', () => {
  let component: MatexpansionPopulationByMigrationComparisonComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByMigrationComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByMigrationComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByMigrationComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
