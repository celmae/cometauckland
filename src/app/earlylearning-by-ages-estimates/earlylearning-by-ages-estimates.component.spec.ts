import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarlylearningByAgesEstimatesComponent } from './earlylearning-by-ages-estimates.component';

describe('EarlylearningByAgesEstimatesComponent', () => {
  let component: EarlylearningByAgesEstimatesComponent;
  let fixture: ComponentFixture<EarlylearningByAgesEstimatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarlylearningByAgesEstimatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarlylearningByAgesEstimatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
