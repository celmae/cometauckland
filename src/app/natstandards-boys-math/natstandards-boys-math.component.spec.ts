import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsBoysMathComponent } from './natstandards-boys-math.component';

describe('NatstandardsBoysMathComponent', () => {
  let component: NatstandardsBoysMathComponent;
  let fixture: ComponentFixture<NatstandardsBoysMathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsBoysMathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsBoysMathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
