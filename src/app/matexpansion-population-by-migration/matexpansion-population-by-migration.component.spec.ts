import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationByMigrationComponent } from './matexpansion-population-by-migration.component';

describe('MatexpansionPopulationByMigrationComponent', () => {
  let component: MatexpansionPopulationByMigrationComponent;
  let fixture: ComponentFixture<MatexpansionPopulationByMigrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationByMigrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationByMigrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
