import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-by-migration',
  templateUrl: './matexpansion-population-by-migration.component.html',
  styleUrls: ['./matexpansion-population-by-migration.component.css']
})
export class MatexpansionPopulationByMigrationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
