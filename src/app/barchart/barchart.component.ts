import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css'],
  providers: [GraphDatabaseJsonserviceService]
})
export class BarchartComponent implements OnInit {

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['Natural Increase', 'Net Migration', 'Population change'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    { data: [13500, 30800, 44400], label: 'Auckland' },
    { data: [28200, 69100, 97300], label: 'New Zealand' },
   
  ];


  // colors
public barChartColors:Array<any> = [
  { // first color
    backgroundColor: 'rgb(128, 212, 255)',
    borderColor: 'rgb(128, 212, 255)',
    pointBackgroundColor: 'rgb(128, 212, 255)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgb(128, 212, 255)'
  },
  { // second color
    backgroundColor: 'rgb(255, 221, 153)',
    borderColor: 'rgb(255, 221, 153)',
    pointBackgroundColor: 'rgb(255, 221, 153)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgb(255, 221, 153)'
  }

];

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    // Only Change 3 values
    let data = [
      Math.round(Math.random() * 100),
      59,
      80,
      (Math.random() * 100),
      56,
      (Math.random() * 100),
      40];
    let clone = JSON.parse(JSON.stringify(this.barChartData));
    clone[0].data = data;
    this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
  }

  constructor() { }

  ngOnInit() {
  }



}