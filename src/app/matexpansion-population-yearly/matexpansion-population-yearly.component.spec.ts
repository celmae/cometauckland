import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionPopulationYearlyComponent } from './matexpansion-population-yearly.component';

describe('MatexpansionPopulationYearlyComponent', () => {
  let component: MatexpansionPopulationYearlyComponent;
  let fixture: ComponentFixture<MatexpansionPopulationYearlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionPopulationYearlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionPopulationYearlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
