import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion-population-yearly',
  templateUrl: './matexpansion-population-yearly.component.html',
  styleUrls: ['./matexpansion-population-yearly.component.css']
})
export class MatexpansionPopulationYearlyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
