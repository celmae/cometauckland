import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material';

@Component({
  selector: 'app-matexpansion',
  templateUrl: './matexpansion.component.html',
  styleUrls: ['./matexpansion.component.css']
})
export class MatexpansionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
