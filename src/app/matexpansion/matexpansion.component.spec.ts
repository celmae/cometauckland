import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexpansionComponent } from './matexpansion.component';

describe('MatexpansionComponent', () => {
  let component: MatexpansionComponent;
  let fixture: ComponentFixture<MatexpansionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexpansionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexpansionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
