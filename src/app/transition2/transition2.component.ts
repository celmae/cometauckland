import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';
import { ChartDataService } from '../services/chart-data.service';

@Component({
  selector: 'app-transition2',
  templateUrl: './transition2.component.html',
  styleUrls: ['./transition2.component.css'],
})
export class Transition2Component implements OnInit {

  // private CATEGORY_API_URL = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/transition2/1-FHlr4TErn-zV0mGB6rLkn_AyyzBFj7Atlouw7FvoCA';
  populateData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartLabels: any[];
  chartType: string = 'pie';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private chartDataService: ChartDataService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('Transition2', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        this.selectedCategory1 = this.categorySelection[0].value;
      
      });

    this.configData.getSelection('Transition2', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;

      this.populateData = this.chartDataService.transitionHighestQualiData;
      this.mapJsonToPopulationDataChart();
    });
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populateData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    //  const selectedData2 = this.populateData
    //  .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_noqualification'], selectedData1[0]['Label_bothL1andL2'],
        selectedData1[0]['Label_bothL3andL4'], selectedData1[0]['Label_level5orlevel6'],
        selectedData1[0]['Label_bachelorandLevel7'], selectedData1[0]['Label_postgraduate'], selectedData1[0]['Label_overseas']];
        
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    this.chartLabels = ['No Qualification', 'Both L1 and L2', 'Both L3 and L4', 'Level 5 or Level 6 Diploma', 'Bachelor Degree and Level 7 Qualification', 'Postgraduate degree', 'Overseas Secondary School Qualification'];

    // add chart data    


    // console.log(JSON.stringify(this.chartData));
    // console.log(JSON.stringify(this.chartLabels));
  }
}
