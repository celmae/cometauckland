import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NatstandardsGirlsReadingComponent } from './natstandards-girls-reading.component';

describe('NatstandardsGirlsReadingComponent', () => {
  let component: NatstandardsGirlsReadingComponent;
  let fixture: ComponentFixture<NatstandardsGirlsReadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NatstandardsGirlsReadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NatstandardsGirlsReadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
